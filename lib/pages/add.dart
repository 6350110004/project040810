import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_firebase_auth/pages/drawer.dart';
import 'package:flutter_firebase_auth/pages/btn.dart';

class Add extends StatefulWidget {
  const Add({Key key}) : super(key: key);

  @override
  State<Add> createState() => _AddState();
}

class _AddState extends State<Add> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();
  final TextEditingController _pdate = TextEditingController();
  final TextEditingController _brand = TextEditingController();

  String _txtNull;

  final CollectionReference _products =
      FirebaseFirestore.instance.collection('products');

  final _formKey = GlobalKey<FormState>();
  @override
  var space = SizedBox(height: 20);
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerSide(),
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: Text('Add Product'),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
                top: 100,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(context).viewInsets.bottom + 20),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  // email
                  TextFormField(
                    controller: _nameController,
                    decoration: InputDecoration(
                        labelText: 'Car name',
                        errorText: _txtNull,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15))),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                  ),
                  space,
                  TextFormField(
                    controller: _priceController,
                    decoration: InputDecoration(
                        labelText: 'Price',
                        errorText: _txtNull,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15))),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    keyboardType:
                        const TextInputType.numberWithOptions(decimal: true),
                  ),
                  space,
                  TextFormField(
                    controller: _pdate,
                    decoration: InputDecoration(
                        labelText: 'Mileage',
                        errorText: _txtNull,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15))),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                      keyboardType:
                      const TextInputType.numberWithOptions(decimal: true)
                  ),
                  space,
                  TextFormField(
                    controller: _brand,
                    decoration: InputDecoration(
                        labelText: 'Car brand',
                        errorText: _txtNull,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15))),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 29,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      RaisedButton(
                        color: Colors.red,
                        hoverColor: Colors.red[900],
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        child: Text(
                          'Cancel',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => BtnNav()));
                        },
                      ),
                      RaisedButton(
                          color: Colors.blue,
                          hoverColor: Colors.blue[900],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Text(
                            'Save',
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              await _products
                                  .add({
                                    "name": _nameController.text,
                                    "price":
                                        double.tryParse(_priceController.text),
                                    "mile": _pdate.text,
                                    "brand": _brand.text,
                                  })
                                  .then((value) => print("Product Added"))
                                  .catchError((error) => print(
                                      "Failed to add product: $error" +
                                          _priceController.text));

                              _nameController.text = '';
                              _priceController.text = '';
                              _pdate.text = '';
                              _brand.text = '';

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BtnNav()));
                            }
                          }),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
