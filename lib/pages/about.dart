import 'package:flutter/material.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: Text("ABOUT"),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              sbox(),
              stdGroup(
                assetImage: AssetImage('assets/images/04.jpg'),
                stdID: '635110004',
                stdName: 'นาย ณัฐชนน มณียม',
              ),
              sbox(),
              stdGroup(
                assetImage: AssetImage('assets/images/08.jpg'),
                stdID: '6350110008',
                stdName: 'นางสาว ธมนวรรณ อินทวงศ์',
              ),
              sbox(),
              stdGroup(
                assetImage: AssetImage('assets/images/10.jpg'),
                stdID: '6350110010',
                stdName: 'นาย นราวิชญ์ กิจวงศ์วัฒนา',
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class sbox extends StatelessWidget {
  const sbox({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 24,
    );
  }
}

class stdGroup extends StatelessWidget {
  const stdGroup({
    this.assetImage,
    this.stdID,
    this.stdName,
    Key key,
  }) : super(key: key);

  final AssetImage assetImage;
  final String stdID;
  final String stdName;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 32),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: Colors.yellow,
      ),
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                radius: 30,
                backgroundImage: assetImage,
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            stdID,
            style: TextStyle(
                fontSize: 18,
                color: Colors.black,
                fontWeight: FontWeight.w700,
                letterSpacing: 1.0),
          ),
          SizedBox(
            height: 7,
          ),
          Text(
            stdName,
            style: TextStyle(
                fontSize: 18,
                color: Colors.black,
                fontWeight: FontWeight.w600,
                letterSpacing: 1.0),
          ),
        ],
      ),
    );
  }
}
